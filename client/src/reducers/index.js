import {combineReducers} from 'redux';
import books from './book_reducers';
import user from './user_reducers';

const rootReducer = combineReducers({
    books,
    user
});
export default rootReducer;