import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {getBook, updateBook, clearBooks, deleteBook} from '../../actions';


class EditBook extends PureComponent {
    state = {
        formdata: {
            _id: this.props.match.params.id,
            name: '',
            author: '',
            review: '',
            page: '',
            rating: '',
            price: ''
        }
    };

    handleInput = (event, name) => {
        const newFormDate = {
            ...this.state.formdata
        };
        newFormDate[name] = event.target.value;
        this.setState({
            formdata: newFormDate
        })
    };


    submitForm = (e) => {
        e.preventDefault();
        this.props.dispatch(updateBook(this.state.formdata))

    };
    deletePost = () => {
        this.props.dispatch(deleteBook(this.props.match.params.id))
    };
    redirectUser = () => {
        setTimeout(() => {
            this.props.history.push('/user/user-reviews')
        }, 1000)
    };

    componentWillReceiveProps(nexrProps) {
        let book = nexrProps.books.book;
        console.log(nexrProps);
        this.setState({
            formdata: {
                _id: book._id,
                name: book.name,
                author: book.author,
                review: book.review,
                page: book.page,
                rating: book.rating,
                price: book.price
            }
        })
    }

    componentWillMount() {
        this.props.dispatch(getBook(this.props.match.params.id));
    }
    componentWillUnmount() {
        this.props.dispatch(clearBooks());
    }

    render() {
        let books = this.props.books;
        return (
            <div className="rl_container article">
                {
                    books.updateBook ?
                        <div className="edit_confirm">
                            Post updated , <Link to={`/books/${books.book._id}`}>Click here to your post </Link>
                        </div>
                        : null
                }
                {
                    books.postDeleted ?
                        <div className="red_tag">
                            Post deleted
                            {this.redirectUser()}
                        </div>
                        : null
                }
                <form action="" onSubmit={this.submitForm}>
                    <h2>Edit review</h2>

                    <div className="form_element">
                        <input type="text" placeholder="Enter name" value={this.state.formdata.name}
                               onChange={(event) => this.handleInput(event, 'name')}/>
                    </div>
                    <div className="form_element">
                        <input type="text" placeholder="Enter author" value={this.state.formdata.author}
                               onChange={(event) => this.handleInput(event, 'author')}
                        />
                    </div>
                    <textarea name="" id="" cols="30" rows="10" value={this.state.formdata.review}
                              onChange={(event) => this.handleInput(event, 'review')}
                    />
                    <div className="form_element">
                        <input type="pages" placeholder="Enter pages" value={this.state.formdata.page}
                               onChange={(event) => this.handleInput(event, 'page')}
                        />
                    </div>
                    <div className="form_element">
                        <select name="" id="" value={this.state.formdata.rating}
                                onChange={(event) => this.handleInput(event, 'rating')}
                        >
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div className="form_element">
                        <input type="number" placeholder="Enter price" value={this.state.formdata.price}
                               onChange={(event) => this.handleInput(event, 'price')}
                        />
                    </div>
                    <button type="submit">Edit review</button>
                    <div className="delete_post">
                        <div className="button"
                             onClick={this.deletePost}>
                            Delete review
                        </div>
                    </div>

                </form>
            </div>
        );
    }

    componentWillUnmount() {
        this.props.dispatch(clearBooks())
    }
}

function mapStateToProps(state) {
    return {
        books: state.books
    }
}

export default connect(mapStateToProps)(EditBook);
