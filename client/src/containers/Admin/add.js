import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {addBook, clearBook} from '../../actions';


class AddBook extends Component {

    state = {
        formdata: {
            name: '',
            author: '',
            review: '',
            page: '',
            rating: '',
            price: ''
        }
    };
    showNewBook = (book) => (
        book.post ?
            <div className="conf_link">
                Cool <Link to={`/books/${book.bookId}`}>
                Click to see the post
            </Link>
            </div>
            : null
    );
    handleInput = (event, name) => {
        const newFormDate = {
            ...this.state.formdata
        };
        newFormDate[name] = event.target.value;
        this.setState({
            formdata: newFormDate
        })
    };



    submitForm = (e) => {
        e.preventDefault();
        this.props.dispatch(addBook({
            ...this.state.formdata,
            ownerId: this.props.user.login.id
        }))
    };

    render() {
        return (
            <div className="rl_container article">
                <form action="" onSubmit={this.submitForm}>
                    <h2>Add review</h2>
                    <div className="form_element">
                        <input type="text" placeholder="Enter name" value={this.state.formdata.name}
                               onChange={(event) => this.handleInput(event, 'name')}/>
                    </div>
                    <div className="form_element">
                        <input type="text" placeholder="Enter author" value={this.state.formdata.author}
                               onChange={(event) => this.handleInput(event, 'author')}
                        />
                    </div>
                    <textarea name="" id="" cols="30" rows="10" value={this.state.formdata.review}
                              onChange={(event) => this.handleInput(event, 'review')}
                    />
                    <div className="form_element">
                        <input type="pages" placeholder="Enter pages" value={this.state.formdata.page}
                               onChange={(event) => this.handleInput(event, 'page')}
                        />
                    </div>
                    <div className="form_element">
                        <select name="" id="" value={this.state.formdata.rating}
                                onChange={(event) => this.handleInput(event, 'rating')}
                        >
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div className="form_element">
                        <input type="number" placeholder="Enter price" value={this.state.formdata.price}
                               onChange={(event) => this.handleInput(event, 'price')}
                        />
                    </div>
                    <button type="submit">Add review</button>
                    {
                        this.props.books.newBook ?
                            this.showNewBook(this.props.books.newBook)
                            : null
                    }
                </form>
            </div>
        );
    }

    componentWillUnmount() {
        console.log('s')
        this.props.dispatch(clearBook())
    }
}

function mapStateToProps(state) {
    console.log(state)
    return {
        books: state.books
    }
}

export default connect(mapStateToProps)(AddBook);
